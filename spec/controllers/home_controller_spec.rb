# Tests for home_controller.rb

require 'rails_helper'

describe HomeController, type: :controller do
  describe 'GET index' do
    # This test is a must pass
    before :each do
      sign_out :user
    end

    it 'redirects to new_user_session_path if the user is not logged in' do
      get :index
      expect( response ).to redirect_to new_user_session_path
    end

    it 'renders the index template if the user is logged in' do
      # Must have a signed in user to get index page
      @user = create( :user )
      sign_in @user
      get :index
      expect( response ).to render_template( 'index' )
    end
  end
end
