Devolute Test App
=================================

**This project uses Rails in Backend, Angular in Frontend, Devise for Authentication.**

Links to be included later on:


### Gems, Frameworks, and other libraries included:

- **Rails Backend**
    - [Rails 4.1.1]
    - [pg] Postgresql database
    - [Devise] Server side authentication library
    - [CanCan] Authorization
    - [Rspec] Test Driven framework for our ruby code
    - [yard] Ruby Documentation Tool
- **Angular Frontend**
    - [angularjs-rails]
    - [ng-rails-csrf] Resolves CSRF header on API requests.
    - [angular-rails-templates] Provides mechanism for auto-inserting angular templates into the rails asset pipeline
    - [angular-ui-bootstrap-rails] Includes bootstrap directives provided by the angular-ui team
    - [teaspoon-jasmine] Javascript test runner
    - [jsdoc] JavaScript Documentation Tool
- **Common Items**
    - [Bootstrap-sass] Bootstrap 3 css with SASS support
    - [simplecov] Metrics

### Features Covered:

- Users can register
- Users can login
- Rails User model

## Devlopment Methodology:
- Test Driven (Ruby and JavaScript testing environments)
- Angular used for front-end as:
  - Rails hand off to Angular on sign-in
  - Angular asset pipeline integration
- Travis CI build info

### Requirements:

- Ruby 2.3.1 installed (Can work with ruby versions > 1.9.3)
- Postgres database installed and configured

### Initial Setup:

- Clone the repo
- <code>bundle install</code> to install all the required gems

### Database Creation:

- Add your DB connection details in config/database.yml
- <code>bundle exec rake db:create</code>

### Database Initialization:

- <code>bundle exec rake db:migrate</code>

### Running Tests

- Ruby tests: <code>bundle exec rspec</code>
- Javascript tests: <code>bundle exec teaspoon</code>

### Continuous Integration

- Travis config file has been included and set up with the standard build info and test suite commands, can be used for CI

### Generating Documentation

- <code>bundle exec rake mydoc</code>
